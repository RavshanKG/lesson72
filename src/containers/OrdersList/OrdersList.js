import React, {Component} from 'react';
import {connect} from "react-redux";

import './OrdersList.css';
import OrderItem from "../../components/OrderItem/OrderItem";

import {completeOrder, loadDishes} from "../../store/actions";

class OrdersList extends Component {
  componentDidMount() {
    this.props.loadDishesFromServer();
  }

  render() {
    return (
      <div className="OrdersList">
        <h3 className="OrdersTitle">Orders</h3>
        {this.props.orders ? Object.keys(this.props.orders).map(id => (
          <OrderItem
            key={id}
            id={id}
            order={this.props.orders[id]}
            dishList={this.props.dishList}
            completeOrder={() => this.props.removeOrderFromList(id)}
          />
        )) : null}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    dishList: state.dishList,
    orders: state.orders
  }
};

const mapDispatchToProps = dispatch => {
  return {
    loadDishesFromServer: () => dispatch(loadDishes()),
    removeOrderFromList: (id) => dispatch(completeOrder(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList);