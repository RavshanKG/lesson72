import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import './DishesList.css';
import DishItem from "../../components/DishItem/DishItem";
import Overlay from "../../components/UI/Overlay/Overlay";
import Modal from "../../components/UI/Modal/Modal";
import NewDishForm from "../../components/NewDishForm/NewDishForm";
import Button from "../../components/UI/Button/Button";

import {addNewDish, editDish, loadDishes, removeDish} from "../../store/actions";

class DishesList extends Component {
  state = {
    dishAdding: false,
    dishEditing: false,
    dishEditingId: ''
  };

  toggleAddingModal = () => {
    this.setState(prevState => {
      return {dishAdding: !prevState.dishAdding};
    });
  };

  toggleEditingModal = id => {
    this.setState(prevState => {
      return {dishEditing: !prevState.dishEditing, dishEditingId: id ? id : ''};
    });
  };

  dishAddingHandler = (event, newDish) => {
    event.preventDefault();
    if (newDish) {
      this.props.addDishToServer(newDish);
    }
    this.toggleAddingModal();
  };

  dishEditingHandler = (event, dish, key) => {
    event.preventDefault();
    if (key) {
      this.props.editExistingDish(key, dish);
    }
    this.toggleEditingModal();
  };

  componentDidMount() {
    this.props.loadDishesFromServer();
  }

  render() {
    let modal = null;

    if (this.state.dishAdding) modal = (
      <Overlay>
        <Modal title={'Add new dish'}>
          <NewDishForm
            okBtn={'CREATE'}
            clicked={(e, dish) => this.dishAddingHandler(e, dish)}
          />
        </Modal>
      </Overlay>
    );

    if (this.state.dishEditing) modal = (
      <Overlay>
        <Modal title={'Edit dish'}>
          <NewDishForm
            okBtn={'SAVE'}
            dish={this.props.dishList[this.state.dishEditingId]}
            id={this.state.dishEditingId}
            clicked={(e, dish, id) => this.dishEditingHandler(e, dish, id)}
          />
        </Modal>
      </Overlay>
    );

    return (
      <Fragment>
        <div className="DishesList">
          <div className="HeadRow">
            <h3 className="Title">Dishes</h3>
            <Button clicked={this.toggleAddingModal}>Add new Dish</Button>
          </div>
          {this.props.dishList ? Object.keys(this.props.dishList).map(id => (
            <DishItem key={id}
                      id={id}
                      dish={this.props.dishList[id]}
                      removeDish={() => this.props.removeDishFromServer(id)}
                      editDish={() => this.toggleEditingModal(id)}
            />
          )) : null}
        </div>
        {modal}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    dishList: state.dishList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addDishToServer: (newDish) => dispatch(addNewDish(newDish)),
    loadDishesFromServer: () => dispatch(loadDishes()),
    removeDishFromServer: (key) => dispatch(removeDish(key)),
    editExistingDish: (key, dish) => dispatch(editDish(key, dish))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DishesList);