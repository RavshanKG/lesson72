import * as actionTypes from './actionTypes';

const initialState = {
  dishList: {},
  orders: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_DISHES_LIST:
      return {...state, dishList: action.dishList};
    case actionTypes.REMOVE_DISH_ITEM:
      let dishList = {...state.dishList};
      delete dishList[action.key];
      return {...state, dishList};
    case actionTypes.LOAD_ORDERS_LIST:
      return {...state, orders: action.orders};
    case actionTypes.COMPLETE_EXISTING_ORDER:
      const orders = {...state.orders};
      delete orders[action.id];
      return {...state, orders};
    default:
      return state;
  }
};

export default reducer;