import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
  return (
    <div className="OrderItem">
      <div className="OrderInfo">
        {Object.keys(props.order).map(dishId => (
          <div className="OrderDish" key={dishId}>
            <span className="DishName">{props.order[dishId]} x {props.dishList[dishId].name}</span>
            <span className="DishValue">{props.order[dishId] * props.dishList[dishId].cost} KGS</span>
          </div>
        ))}
        <div className="OrderDish Delivery">
          <span className="DishName">Delivery</span>
          <span className="DishValue">150 KGS</span>
        </div>
      </div>
      <div className="OrderSummary">
        <p className="OrderSummaryRow">Order total:</p>
        <p className="OrderSummaryRow"><strong>
          {Object.keys(props.order).reduce(((sum, el) => sum + parseInt(props.dishList[el].cost, 10)), 0) + 150} KGS
        </strong>
        </p>
        <p className="OrderSummaryRow OrderComplete" onClick={props.completeOrder}>Complete order</p>
      </div>
    </div>
  )
};

export default OrderItem;