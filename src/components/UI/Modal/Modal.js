import React from 'react';
import './Modal.css';

const Modal = props => {
  return (
    <div className="Modal">
      <div className="Title">{props.title}</div>
      {props.children}
    </div>
  )
};

export default Modal;