import React from 'react';
import './Toolbar.css';
import {NavLink} from "react-router-dom";

const Toolbar = () => (
  <header className="Toolbar">
    <h2 className="Title">Turtle Pizza Admin</h2>
    <div className="NavBar">
      <NavLink className="Toolbar-Item" activeClassName="Selected"
               to='/' exact>Dishes</NavLink>
      <NavLink className="Toolbar-Item" activeClassName="Selected"
               to='/orders'>Orders</NavLink>
    </div>
  </header>
);

export default Toolbar;